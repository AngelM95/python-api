from django.db import models


class Publishable(models.Model):
    publish_date = models.DateTimeField(null=True)

    class Meta:
        abstract = True


class Timestampable(models.Model):

    create_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Activable(models.Model):
    active = models.BooleanField(default=True)

    class Meta:
        abstract = True


class Showable(models.Model):
    name = models.CharField(max_length=50, unique=True)
    description = models.TextField(null=True)

    class Meta:
        abstract = True


class Category(Activable, Showable):
    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Categoria"


class SubCategory(Activable, Showable):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Subcategoria"


class Product(Timestampable, Publishable, Activable, Showable):
    subcategory = models.ForeignKey(SubCategory, on_delete=models.CASCADE)
    price = models.DecimalField(default=0.0, max_digits=10, decimal_places=2)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Producto"
