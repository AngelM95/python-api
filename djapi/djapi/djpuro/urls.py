from django.urls import path
from .views import category_list, subcategory_list, category_detail

urlpatterns = [
    path('categories/', category_list, name='category_list'),
    path('categories/<int:id>', category_detail, name='category_detail'),
    path('subcategories/', subcategory_list, name='subcategory_list')
]
