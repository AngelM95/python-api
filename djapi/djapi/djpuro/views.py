from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from .models import Category, SubCategory

# Create your views here.


def category_list(request):
    data = Category.objects.all().values('id', 'active', 'name', 'description')
    return JsonResponse(list(data), safe=False)


def subcategory_list(request):
    data = SubCategory.objects.all().values()
    return JsonResponse(list(data), safe=False)


def category_detail(request, id):
    cat = get_object_or_404(Category, pk=id)
    data = {
        'active': cat.active,
        'name': cat.name,
        'description': cat.name
    }
    return JsonResponse(data)
