from rest_framework import generics, viewsets
from .serializers import *
from .models import Category, SubCategory, Product


# Create your views here.


class CategoryList(generics.ListCreateAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class CategoryDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class SubCategoryList(generics.ListAPIView):
    def get_queryset(self):
        queryset = SubCategory.objects.filter(category_id=self.kwargs['pk'])
        return queryset

    serializer_class = SubCategorySerializer


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class UserCreate(generics.CreateAPIView):
    serializer_class = UserSerializer
