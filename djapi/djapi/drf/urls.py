from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import CategoryList, SubCategoryList, CategoryDetail, ProductViewSet, UserCreate

urlpatterns = [
    path('categories/', CategoryList.as_view(), name='category_list'),
    path('categories/<int:pk>/subcategories/',
         SubCategoryList.as_view(), name='subcategory_list'),
    path('categories/<int:pk>/',
         CategoryDetail.as_view(), name='subcategory_detail'),
    path('users/',
         UserCreate.as_view(), name='user_create')
]

router = DefaultRouter()
router.register('products', ProductViewSet, base_name='products')
urlpatterns += router.urls
